﻿using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using WebStoreApp.Models;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Facebook;
using System.Web.Configuration;
using Microsoft.Owin.Security;
using System;

[assembly: OwinStartup(typeof(AspNetIdentityApp.Startup))]

namespace AspNetIdentityApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<ApplicationContext>(ApplicationContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = "Google",
                ClientId = "644850337544-7s3dj7pe2g90aoup5njfmute82epso6u.apps.googleusercontent.com",
                ClientSecret = "FJ58APU1AsUWbd9U4B5qZdpL",
                Caption = "Google+ Authorization",
                CallbackPath = new PathString("/GoogleLoginCallback"),
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
                SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType(),
                BackchannelTimeout = TimeSpan.FromSeconds(60),
                BackchannelHttpHandler = new System.Net.Http.WebRequestHandler(),
                BackchannelCertificateValidator = null,
                Provider = new GoogleOAuth2AuthenticationProvider()
            });

            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AuthenticationType = "Facebook",
                AppId = "438581833581543",
                AppSecret = "185c3f801c4be6e01bb1aefd07c6588a",
                Caption = "Facebook Authorization",
                CallbackPath = new PathString("/FacebookLoginCallback"),
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
                SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType(),
                BackchannelTimeout = TimeSpan.FromSeconds(60),
                BackchannelHttpHandler = new System.Net.Http.WebRequestHandler(),
                BackchannelCertificateValidator = null,
                Provider = new FacebookAuthenticationProvider()
            });
        }
    }
}