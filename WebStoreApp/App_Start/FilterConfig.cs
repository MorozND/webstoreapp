﻿using System.Web;
using System.Web.Mvc;
using WebStoreApp.Filters;

namespace WebStoreApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AuthorizeAttribute());
            filters.Add(new RequireHttpsAttribute());

            // custom filters
            filters.Add(new RouteLogAttribute());
            filters.Add(new CustomExceptionAttribute());
        }
    }
}
