namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedAdminRole : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [Surname], [DateRegistered], [FullName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7e413d6e-40a1-43be-9f0b-97a5260cb37c', N'Admin`', N'Adminovich', N'2019-04-12 19:17:37', N'Admin` Adminovich', N'morozishko95@mail.ru', 1, N'AFmG1UZmHLHXdCGX1QCFdesJxDtjZScupZ4I2OyvJBRF8cz4QQK6Zak59OVbSByZ0w==', N'7bc0758d-47de-45f6-a349-c8d6e73be031', NULL, 0, 0, NULL, 0, 0, N'morozishko95@mail.ru')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'4495865e-adac-41d1-bb7e-35d1a778ab40', N'Admin', N'IdentityRole')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7e413d6e-40a1-43be-9f0b-97a5260cb37c', N'4495865e-adac-41d1-bb7e-35d1a778ab40')
            ");
        }
        
        public override void Down()
        {

        }
    }
}
