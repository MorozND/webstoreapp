namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserOrdersOneToMany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserOrders", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserOrders", "Order_Id", "dbo.Orders");
            DropIndex("dbo.UserOrders", new[] { "User_Id" });
            DropIndex("dbo.UserOrders", new[] { "Order_Id" });
            AddColumn("dbo.Orders", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "UserId");
            AddForeignKey("dbo.Orders", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropTable("dbo.UserOrders");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserOrders",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Order_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Order_Id });
            
            DropForeignKey("dbo.Orders", "UserId", "dbo.Users");
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropColumn("dbo.Orders", "UserId");
            CreateIndex("dbo.UserOrders", "Order_Id");
            CreateIndex("dbo.UserOrders", "User_Id");
            AddForeignKey("dbo.UserOrders", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserOrders", "User_Id", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
