namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddManagerRole : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [dbo].[AspNetRoles] ([Id], [Name], [Discriminator]) VALUES (N'505330bd-46d3-4737-9283-b29b874d991f', N'Manager', N'IdentityRole')");
        }
        
        public override void Down()
        {
        }
    }
}
