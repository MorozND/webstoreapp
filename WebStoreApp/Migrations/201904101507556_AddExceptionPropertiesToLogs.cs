namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExceptionPropertiesToLogs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "StackTrace", c => c.String());
            AddColumn("dbo.Logs", "ErrorMessage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Logs", "ErrorMessage");
            DropColumn("dbo.Logs", "StackTrace");
        }
    }
}
