namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveCountFromOrder : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "Count");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Count", c => c.Int(nullable: false));
        }
    }
}
