namespace WebStoreApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogstable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        LogId = c.Int(nullable: false, identity: true),
                        RequestTime = c.DateTime(),
                        RequestUrl = c.String(),
                        UserName = c.String(),
                        Headers = c.String(),
                        Body = c.String(),
                        QueryString = c.String(),
                        HttpVerb = c.String(),
                        StatusCode = c.String(),
                    })
                .PrimaryKey(t => t.LogId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Logs");
        }
    }
}
