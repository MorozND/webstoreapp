﻿using System.Web.Mvc;
using WebStoreApp.Customs;

namespace WebStoreApp.Filters
{
    public class CustomExceptionAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var requestInfo = new RequestInfo(filterContext.HttpContext);

            var requestTime = requestInfo.RequestTime;
            var requestUrl = requestInfo.RequestUrl;
            var userName = requestInfo.UserName;
            var headers = requestInfo.Headers.ToString();
            var body = requestInfo.Body;
            var queryString = requestInfo.QueryString;
            var httpVerb = requestInfo.HttpVerb;

            string stackTrace = filterContext.Exception.StackTrace;
            string errorMessage = filterContext.Exception.Message;

            log4net.LogicalThreadContext.Properties["requestTime"] = requestTime;
            log4net.LogicalThreadContext.Properties["requestUrl"] = requestUrl;
            log4net.LogicalThreadContext.Properties["username"] = userName;
            log4net.LogicalThreadContext.Properties["headers"] = headers;
            log4net.LogicalThreadContext.Properties["body"] = body;
            log4net.LogicalThreadContext.Properties["queryString"] = queryString;
            log4net.LogicalThreadContext.Properties["httpVerb"] = httpVerb;
            log4net.LogicalThreadContext.Properties["stackTrace"] = stackTrace;
            log4net.LogicalThreadContext.Properties["errorMessage"] = errorMessage;

            MvcApplication.logger.Error("");

            log4net.LogicalThreadContext.Properties.Clear();
        }
    }
}