﻿using System;
using System.Web.Mvc;
using WebStoreApp.Customs;

namespace WebStoreApp.Filters
{
    public class RouteLogAttribute : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var requestInfo = new RequestInfo(filterContext.HttpContext);

            var requestTime = requestInfo.RequestTime;
            var requestUrl = requestInfo.RequestUrl;
            var userName = requestInfo.UserName;
            var headers = requestInfo.Headers.ToString();
            var body = requestInfo.Body;
            var queryString = requestInfo.QueryString;
            var httpVerb = requestInfo.HttpVerb;

            log4net.LogicalThreadContext.Properties["requestTime"] = requestTime;
            log4net.LogicalThreadContext.Properties["requestUrl"] = requestUrl;
            log4net.LogicalThreadContext.Properties["username"] = userName;
            log4net.LogicalThreadContext.Properties["headers"] = headers;
            log4net.LogicalThreadContext.Properties["body"] = body;
            log4net.LogicalThreadContext.Properties["queryString"] = queryString;
            log4net.LogicalThreadContext.Properties["httpVerb"] = httpVerb;

            MvcApplication.logger.Info("");

            log4net.LogicalThreadContext.Properties.Clear();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var requestInfo = new RequestInfo(filterContext.HttpContext);

            var requestTime = requestInfo.RequestTime;
            var requestUrl = requestInfo.RequestUrl;
            var userName = requestInfo.UserName;
            var headers = requestInfo.Headers.ToString();
            var body = requestInfo.Body;
            var statusCode = requestInfo.StatusCode;

            log4net.LogicalThreadContext.Properties["requestTime"] = requestTime;
            log4net.LogicalThreadContext.Properties["requestUrl"] = requestUrl;
            log4net.LogicalThreadContext.Properties["username"] = userName;
            log4net.LogicalThreadContext.Properties["headers"] = headers;
            log4net.LogicalThreadContext.Properties["body"] = body;
            log4net.LogicalThreadContext.Properties["statusCode"] = statusCode;

            MvcApplication.logger.Info("");

            log4net.LogicalThreadContext.Properties.Clear();
        }
    }
}