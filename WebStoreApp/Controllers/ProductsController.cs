﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStoreApp.Models;
using WebStoreApp.ViewModels;

namespace WebStoreApp.Controllers
{
    public class ProductsController : Controller
    {
        ApplicationContext _context;

        public ProductsController()
        {
            _context = new ApplicationContext();
        }
        // GET: Products
        public ActionResult Index()
        {
            return View(_context.Products.ToList());
        }

        public ViewResult Add()
        {
            return View("ProductForm", new ProductFormViewModel());
        }

        public ActionResult Edit(int id)
        {
            var productFromDb = _context.Products.Find(id);

            if (productFromDb == null)
                return HttpNotFound();

            return View("ProductForm", new ProductFormViewModel(productFromDb));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ProductFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("ProductForm", viewModel);

            if (viewModel.Id == 0) // ADD
            {
                var product = new Product()
                {
                    Name = viewModel.Name,
                    Price = viewModel.Price ?? 0,
                    Description = viewModel.Description,
                    DateAdded = viewModel.DateAdded ?? DateTime.Now
                };

                _context.Products.Add(product);
            }
            else // EDIT
            {
                var productFromDb = _context.Products.Find(viewModel.Id);

                if (productFromDb == null)
                    return HttpNotFound();

                // update information
                productFromDb.Name = viewModel.Name;
                productFromDb.Price = viewModel.Price ?? 0;
                productFromDb.Description = viewModel.Description;
                productFromDb.DateAdded = viewModel.DateAdded ?? DateTime.Now;
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}