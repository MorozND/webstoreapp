﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebStoreApp.Models;
using WebStoreApp.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using WebStoreApp.Customs;

namespace WebStoreApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        #region Private properties
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationContext _context;
        #endregion

        public AccountController()
        {
            _context = new ApplicationContext();
        }

        public ViewResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            var user = new AspNetUser
            {
                UserName = viewModel.Email,
                Email = viewModel.Email,
                Name = viewModel.Name,
                Surname = viewModel.Surname,
                DateRegistered = DateTime.Now
            };

            IdentityResult result = await UserManager.CreateAsync(user, viewModel.Password);

            if (result.Succeeded)
            {
                // create token for Email confirmation
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                // create confirm link
                var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code },
                        protocol: Request.Url.Scheme
                    );

                // send Email
                await UserManager.SendEmailAsync(
                        user.Id, 
                        "Подтверждение электронной почты",
                        $"Для завершения регистрации перейдите по ссылке: <a href='{callbackUrl}'>Завершить регистрацию</a>"
                    );

                return View("DisplayEmail");
            }
            else
            {
                foreach (string error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }

                return View(viewModel);
            }
        }

        public ViewResult LogIn()
        {
            return View(new LogInViewModel());
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> LogIn(LogInViewModel viewModel)
        {
            // add to role
            var roleStore = new RoleStore<IdentityRole>(new ApplicationContext());
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            await roleManager.CreateAsync(new IdentityRole(RoleName.Manager));

            if (!ModelState.IsValid)
                return View(viewModel);

            AspNetUser user = await UserManager.FindAsync(viewModel.Email, viewModel.Password);

            if (user != null)
            {
                if (user.EmailConfirmed == true)
                {
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(
                            user, DefaultAuthenticationTypes.ApplicationCookie
                        );

                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(
                            new AuthenticationProperties
                            {
                                IsPersistent = true
                            },
                            claim
                        );

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Email not confirmed");
                }
            }
            else
            {
                ModelState.AddModelError("", "Неверный логин или пароль");
            }

            return View(viewModel);
        }

        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
                return View("Error");

            var result = await UserManager.ConfirmEmailAsync(userId, code);

            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        #region Google Authentication
        [HttpPost]
        public ActionResult GoogleLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("GoogleLoginCallback",
                    new { returnUrl = returnUrl })
            };

            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Google");
            return new HttpUnauthorizedResult();
        }

        public async Task<ActionResult> GoogleLoginCallback(string returnUrl)
        {
            ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            AspNetUser user = await UserManager.FindAsync(loginInfo.Login);

            if (user == null)
            {
                user = new AspNetUser
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.DefaultUserName,
                    DateRegistered = DateTime.Now
                };

                IdentityResult result = await UserManager.CreateAsync(user);
                if (!result.Succeeded)
                {
                    return View("Error", result.Errors);
                }
                else
                {
                    result = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                    if (!result.Succeeded)
                    {
                        return View("Error", result.Errors);
                    }
                }
            }

            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
                DefaultAuthenticationTypes.ApplicationCookie);

            ident.AddClaims(loginInfo.ExternalIdentity.Claims);

            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, ident);

            return Redirect(returnUrl ?? "/");
        }
        #endregion

        #region Facebook Authentication
        [HttpPost]
        public ActionResult FacebookLogin(string returnUrl)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("FacebookLoginCallback",
                    new { returnUrl = returnUrl })
            };

            HttpContext.GetOwinContext().Authentication.Challenge(properties, "Facebook");
            return new HttpUnauthorizedResult();
        }

        public async Task<ActionResult> FacebookLoginCallback(string returnUrl)

        {
            ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            var checkUser = await UserManager.FindByEmailAsync(loginInfo.Email);
            if (checkUser == null)
            {
                 checkUser = new AspNetUser
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.DefaultUserName,
                    DateRegistered = DateTime.Now
                };
                IdentityResult checkResult = await UserManager.CreateAsync(checkUser);
                if (!checkResult.Succeeded)
                {
                    return View("Error", checkResult.Errors);
                }
                await AddExternalUserInfo(checkUser, loginInfo);
            }
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user == null)
            {

                await AddExternalUserInfo(checkUser, loginInfo);

            }
            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
               DefaultAuthenticationTypes.ApplicationCookie);

            ident.AddClaims(loginInfo.ExternalIdentity.Claims);

            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, ident);
            return Redirect(returnUrl ?? "/");
        }
        #endregion

        private async Task AddExternalUserInfo(AspNetUser user, ExternalLoginInfo loginInfo)
        {
            var result = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);           
        }

        //public ViewResult PromoteToManager()
        //{
        //    var managerRoleId = _context.Roles.SingleOrDefault(r => r.Name == RoleName.Manager).Id;

        //    var viewModel = new PromoteViewModel()
        //    {
        //        Users = _context.Users.Where(u => u.Roles.Any())
        //    }                

        //    return View(viewModel);
        //}

        //[HttpPost]
        //public async Task<ActionResult> PromoteToManager(string userId)
        //{
        //    var userFromDb = await UserManager.FindByIdAsync(userId);

        //    if (userFromDb == null)
        //        return HttpNotFound();

        //    var result = await UserManager.AddToRolesAsync(userId, RoleName.Manager);

        //    ViewBag.Msg = result.Succeeded
        //        ? "Success!"
        //        : "Error";

        //    return View("PromoteToManager", );
        //}
    }
}