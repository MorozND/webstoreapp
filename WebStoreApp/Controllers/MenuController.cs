﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStoreApp.Models;

namespace WebStoreApp.Controllers
{
    public class MenuController : Controller
    {
        private List<MenuItem> _items;

        public MenuController()
        {
            _items = new List<MenuItem>
            {
                new MenuItem
                {
                    Name = "Products",
                    Controller = "Products",
                    Action = "Index",
                    Active = string.Empty
                },
                new MenuItem
                {
                    Name = "Orders",
                    Controller = "Orders",
                    Action = "Index",
                    Active = string.Empty
                }
            };
        }

        public PartialViewResult Main(string controller = "Home")
        {
            var item = _items.FirstOrDefault(i => i.Controller == controller);

            if (item != null)
                item.Active = "active";

            return PartialView(_items);
        }
    }
}