﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStoreApp.Models;
using WebStoreApp.ViewModels;

namespace WebStoreApp.Controllers
{
    public class UsersController : Controller
    {
        ApplicationContext _context;

        public UsersController()
        {
            _context = new ApplicationContext();
        }

        // GET: Users
        public ActionResult Index()
        {
            return View(_context.Users.ToList());
        }

        public ViewResult Add()
        {
            return View("UserForm", new UserFormViewModel());
        }

        public ActionResult Edit(int id)
        {
            var userFromDb = _context.Users.Find(id);

            if (userFromDb == null)
                return HttpNotFound();

            return View("UserForm", new UserFormViewModel(userFromDb));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("UserForm", viewModel);

            if (viewModel.Id == "0") // ADD
            {
                var user = new AspNetUser()
                {
                    Name = viewModel.Name,
                    Surname = viewModel.Surname,
                    DateRegistered = viewModel.DateRegistered ?? DateTime.Now,
                    Email = viewModel.Email
                };

                _context.Users.Add(user);
            }
            else // EDIT
            {
                var userFromDb = _context.Users.Find(viewModel.Id);

                if (userFromDb == null)
                    return HttpNotFound();

                // update properties
                userFromDb.Name = viewModel.Name;
                userFromDb.Surname = viewModel.Surname;
                userFromDb.Email = viewModel.Email;
                userFromDb.DateRegistered = viewModel.DateRegistered ?? DateTime.Now;
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}