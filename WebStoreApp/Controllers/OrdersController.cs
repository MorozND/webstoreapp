﻿using System;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebStoreApp.Models;
using WebStoreApp.ViewModels;
using WebStoreApp.Customs;
using Microsoft.AspNet.Identity;
using System.Threading;

namespace WebStoreApp.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        ApplicationContext _context;

        public OrdersController()
        {
            _context = new ApplicationContext();
        }

        // GET: Orders
        public ActionResult Index()
        {
            List<OrderViewModel> model = new List<OrderViewModel>();

            foreach (var order in _context.Orders.ToList())
            {
                var userId = User.Identity.GetUserId();
                if (order.UserId == userId)
                {
                    var viewModel = new OrderViewModel(order);
                    viewModel.Price = CountOrderPrice(order);

                    model.Add(viewModel);
                }
            }

            return View(model);
        }

        public ViewResult Add()
        { 
            return View(PrepareOrderFormViewModel());
        }

        //[ValidateAntiForgeryToken]
        public ActionResult Save(OrderFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View("Add", viewModel);

            // get user
            var userFromDb = _context.Users.Find(User.Identity.GetUserId());
            if (userFromDb == null)
                return HttpNotFound();

            // create order
            var order = new Order
            {
                DateBought = viewModel.DateBought ?? DateTime.Now
            };
            userFromDb.Orders.Add(order);

            // add products with ammounts
            bool isProductChosen = false;
            foreach (var item in viewModel.ProductsAmmount)
            {
                if (item.Ammount != 0)
                {
                    var productFromDb = _context.Products.SingleOrDefault(p => p.Id == item.ProductId);
                    if (productFromDb == null)
                        return HttpNotFound();

                    order.OrdersProducts.Add(new OrdersProducts
                        {
                            Product = productFromDb,
                            ProductAmmount = item.Ammount
                        }
                    );

                    isProductChosen = true;
                }
            }

            if (!isProductChosen)
            {
                ModelState.AddModelError("", "You should choose at least one product");
                return View("Add", PrepareOrderFormViewModel());
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ViewResult Period()
        {
            return View(new PeriodViewModel());
        }

        public PartialViewResult GetPeriodInfo(PeriodViewModel viewModel)
        {
            var orders = _context.Orders.Where(o => 
                                            o.DateBought >= viewModel.StartDate 
                                            && o.DateBought <= viewModel.EndDate
                                            )
                                            .ToList();

            double totalPrice = 0;
            foreach (var order in orders)
            {
                totalPrice += CountOrderPrice(order);
            }

            return PartialView("_PeriodInfo", new PeriodInfoViewModel { SalesNumber = orders.Count, Price = totalPrice });
        }

        //public ViewResult UserOrder()
        //{
        //    var viewModel = new UserOrderViewModel()
        //    {
        //        Users = _context.Users.OrderBy(u => u.FullName).ToList()
        //    };

        //    return View(viewModel);

        //}

        //public PartialViewResult GetUserOrderInfo()
        //{
        //    // get User's orders
        //    var userId = User.Identity.GetUserId();
        //    var orders = _context.Orders.Where(o => o.UserId == userId).ToList();

        //    if (orders == null || orders.Count == 0)
        //    {
        //        return PartialView("_UserOrderInfo", null);
        //    }

        //    // find the products for each order with their ammounts
        //    var orderInfos = new List<OrderInfo>();

        //    foreach (var order in orders)
        //    {
        //        var orderProducts = _context.OrdersProducts.Where(op => op.OrderId == order.Id).ToList();

        //        var orderInfo = new OrderInfo(order);

        //        foreach (var item in orderProducts)
        //        {
        //            var productAmmount = new ProductAmmount(_context.Products.SingleOrDefault(p => p.Id == item.ProductId));
        //            productAmmount.Ammount = item.ProductAmmount;

        //            orderInfo.ProductAmmounts.Add(productAmmount);
        //            orderInfo.OrderPrice = CountOrderPrice(order);
        //        }

        //        orderInfos.Add(orderInfo);
        //    }

        //    // prepare the model
        //    var model = new UserOrderInfoViewModel()
        //    {
        //        Orders = orderInfos,
        //        TotalPrice = CountOrdersPrice(orders)
        //    };

        //    return PartialView("_UserOrderInfo", model);
        //}

        public PartialViewResult GetUserOrderInfo(int id)
        {
            var order = _context.Orders.SingleOrDefault(o => o.Id == id);

            if (order == null)
                return PartialView("_UserOrderInfo", null);

            // find the products for this order and their ammount
            var orderInfo = new OrderInfo(order);

            var orderProducts = _context.OrdersProducts.Where(op => op.OrderId == order.Id).ToList();

            foreach (var item in orderProducts)
            {
                var productAmmount = new ProductAmmount(_context.Products.SingleOrDefault(p => p.Id == item.ProductId));
                productAmmount.Ammount = item.ProductAmmount;

                orderInfo.ProductAmmounts.Add(productAmmount);
                orderInfo.OrderPrice = CountOrderPrice(order);
            }

            return PartialView("_UserOrderInfo", orderInfo);
        }

        #region Private helpers
        private double CountOrderPrice(Order order)
        {
            double result = 0;

            foreach (var productOrder in order.OrdersProducts)
            {
                result += (productOrder.Product.Price * productOrder.ProductAmmount);
            }

            return result;
        }

        private double CountOrdersPrice(IEnumerable<Order> orders)
        {
            double totalPrice = 0;

            foreach (var order in orders)
            {
                totalPrice += CountOrderPrice(order);
            }

            return totalPrice;
        }

        private OrderFormViewModel PrepareOrderFormViewModel()
        {
            var productsAmmount = new List<ProductAmmount>();

            foreach (var product in _context.Products.ToList())
            {
                productsAmmount.Add(new ProductAmmount(product));
            }

            var viewModel = new OrderFormViewModel()
            {
                ProductsAmmount = productsAmmount
            };

            return viewModel;
        }
        #endregion
    }
}