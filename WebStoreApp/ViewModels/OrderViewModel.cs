﻿using System;
using WebStoreApp.Models;

namespace WebStoreApp.ViewModels
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        public double Price { get; set; }

        public OrderViewModel()
        { }

        public OrderViewModel(Order order)
        {
            Id = order.Id;
            DateBought = order.DateBought;
        }
    }
}