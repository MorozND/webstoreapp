﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStoreApp.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Repeat Password")]
        [Compare("Password", ErrorMessage = "Enter the same password")]
        public string PasswordRepeat { get; set; }
    }
}