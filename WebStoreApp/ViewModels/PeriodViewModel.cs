﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebStoreApp.ViewModels
{
    public class PeriodViewModel
    {
        [Required(ErrorMessage = "Enter the Start Date")]
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "Enter the End Date")]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
    }
}