﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebStoreApp.Models;
using WebStoreApp.Customs;

namespace WebStoreApp.ViewModels
{
    public class OrderFormViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter the Date")]
        [Display(Name = "Date")]
        public DateTime? DateBought { get; set; }

        [Display(Name = "Products")]
        public List<ProductAmmount> ProductsAmmount { get; set; }
    }
}