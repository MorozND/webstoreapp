﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;

namespace WebStoreApp.ViewModels
{
    public class ProductFormViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter the Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter the Price")]
        public double? Price { get; set; }

        public string Description { get; set; }

        [Display(Name = "Date added")]
        public DateTime? DateAdded { get; set; }

        public string Title { get; set; }

        public ProductFormViewModel()
        {
            Id = 0;
            Title = "Add new product";
        }

        public ProductFormViewModel(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Price = product.Price;
            Description = product.Description;
            DateAdded = product.DateAdded;
            Title = "Edit product";
        }
    }
}