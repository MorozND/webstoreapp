﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;

namespace WebStoreApp.ViewModels
{
    public class UserFormViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Enter the Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter the Surname")]
        public string Surname { get; set; }

        public DateTime? DateRegistered { get; set; }

        [EmailAddress(ErrorMessage = "Enter valid Email address")]
        public string Email { get; set; }

        public string Title { get; set; }

        public UserFormViewModel()
        {
            Id = "0";
            Title = "Add user";
        }

        public UserFormViewModel(AspNetUser user)
        {
            Id = user.Id;
            Name = user.Name;
            Surname = user.Surname;
            DateRegistered = user.DateRegistered;
            Email = user.Email;
            Title = "Edit user";
        }
    }
}