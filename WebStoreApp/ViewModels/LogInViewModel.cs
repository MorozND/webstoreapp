﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebStoreApp.ViewModels
{
    public class LogInViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}