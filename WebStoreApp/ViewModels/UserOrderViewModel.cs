﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;

namespace WebStoreApp.ViewModels
{
    public class UserOrderViewModel
    {
        [Display(Name = "AspNetUser")]
        public string UserId { get; set; }

        public List<AspNetUser> Users { get; set; }
    }
}