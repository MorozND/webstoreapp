﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;
using WebStoreApp.Customs;

namespace WebStoreApp.ViewModels
{
    public class UserOrderInfoViewModel
    {
        public List<OrderInfo> Orders { get; set; }

        public double TotalPrice { get; set; }
    }
}