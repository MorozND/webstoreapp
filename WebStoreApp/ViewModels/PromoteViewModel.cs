﻿using System;
using System.Collections.Generic;
using WebStoreApp.Models;

namespace WebStoreApp.ViewModels
{
    public class PromoteViewModel
    {
        public string UserId { get; set; }

        public List<AspNetUser> Users { get; set; }

        public PromoteViewModel()
        {
            Users = new List<AspNetUser>();
        }
    }
}