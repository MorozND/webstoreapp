﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebStoreApp.ViewModels
{
    public class PeriodInfoViewModel
    {
        [Display(Name = "Sales")]
        public int SalesNumber { get; set; }

        [Display(Name = "Total price")]
        public double Price { get; set; }
    }
}