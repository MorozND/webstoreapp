﻿using System;

namespace WebStoreApp.Customs
{
    public static class RoleName
    {
        public static readonly string Admin = "Admin";

        public static readonly string Manager = "Manager";
    }
}