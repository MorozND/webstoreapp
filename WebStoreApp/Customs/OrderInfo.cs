﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;

namespace WebStoreApp.Customs
{
    public class OrderInfo
    {
        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        public List<ProductAmmount> ProductAmmounts { get; set; }

        public double OrderPrice { get; set; }

        public OrderInfo()
        {
            ProductAmmounts = new List<ProductAmmount>();
        }

        public OrderInfo(Order order) : this()
        {
            Id = order.Id;
            DateBought = order.DateBought;
        }
    }
}