﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebStoreApp.Models;

namespace WebStoreApp.Customs
{
    public class ProductAmmount
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int Ammount { get; set; }

        public ProductAmmount()
        { }

        public ProductAmmount(Product product)
        {
            ProductId = product.Id;
            ProductName = product.Name;
            Ammount = 0;
        }
    }
}