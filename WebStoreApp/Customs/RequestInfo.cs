﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;

namespace WebStoreApp.Customs
{
    public class RequestInfo
    {
        public DateTime RequestTime { get; set; }

        public string RequestUrl { get; set; }

        public string UserName { get; set; }

        public StringBuilder Headers { get; set; }

        public string Body { get; set; }

        public string QueryString { get; set; }

        public string HttpVerb { get; set; }

        public string StatusCode { get; set; }

        public RequestInfo()
        {
            Headers = new StringBuilder();
            RequestTime = DateTime.Now;
        }

        public RequestInfo(HttpContextBase context) : this()
        {
            DateTime requestTime = DateTime.Now;
            RequestUrl = context.Request.Url.PathAndQuery;

            // get headers
            Dictionary<string, string> headers = new Dictionary<string, string>();
            NameValueCollection headersFromRequest = context.Request.Headers;
            if (headersFromRequest != null)
            {
                for (int i = 0; i < headersFromRequest.Count; i++)
                {
                    headers.Add(
                        headersFromRequest.GetKey(i),
                        headersFromRequest.Get(i)
                    );
                }
            }

            if (headers.Count > 0)
            {
                foreach (var item in headers)
                {
                    Headers.AppendLine($"{item.Key} : {item.Value}");
                }
            }

            // get body
            Stream stream = context.Request.InputStream;
            stream.Seek(0, SeekOrigin.Begin);
            Body = new StreamReader(stream).ReadToEnd();

            QueryString = context.Request.Url.Query;
            HttpVerb = context.Request.HttpMethod;
            StatusCode = context.Response.StatusCode.ToString();

            UserName = String.Empty;
            if (context.Request.IsAuthenticated)
            {
                UserName = context.User.Identity.GetUserName();
            }
        }
    }
}