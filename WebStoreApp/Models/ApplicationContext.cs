﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebStoreApp.Models
{
    public class ApplicationContext : IdentityDbContext<AspNetUser>
    {
        public ApplicationContext() : base("DefaultConnection")
        { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrdersProducts> OrdersProducts { get; set; }

        public DbSet<Log> Logs { get; set; }
    }
}