﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebStoreApp.Models
{
    public class Order
    {
        public Order()
        {
            OrdersProducts = new List<OrdersProducts>();
        }

        public int Id { get; set; }

        public DateTime DateBought { get; set; }

        [ForeignKey("User")]
        [Required]
        public string UserId { get; set; }

        // navigation properties
        public virtual List<OrdersProducts> OrdersProducts { get; set; }
        public virtual AspNetUser User { get; set; }
    }
}