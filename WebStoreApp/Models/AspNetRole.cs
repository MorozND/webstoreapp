﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebStoreApp.Models
{
    public class AspNetRole : IdentityRole
    {
        public AspNetRole() : base() { }

        public AspNetRole(string name) : base(name) { } 
    }
}