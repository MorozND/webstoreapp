﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace WebStoreApp.Models
{
    public class ApplicationRoleManager : RoleManager<AspNetRole>
    {
        public ApplicationRoleManager(RoleStore<AspNetRole> store)
            : base(store) { }

        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(new RoleStore<AspNetRole>(context.Get<ApplicationContext>()));
        }
    }
}