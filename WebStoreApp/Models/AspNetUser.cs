﻿using System;
using System.Security.Claims;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace WebStoreApp.Models
{
    public class AspNetUser : IdentityUser
    {
        public AspNetUser()
        {
            Orders = new List<Order>();
        }

        [MaxLength(128)]
        public string Name { get; set; }

        [MaxLength(128)]
        public string Surname { get; set; }

        public DateTime DateRegistered { get; set; }

        public string FullName
        {
            get { return String.Format($"{Name} {Surname}"); }
            set { }
        }

        // navigation properties
        public virtual List<Order> Orders { get; set; }
    }
}