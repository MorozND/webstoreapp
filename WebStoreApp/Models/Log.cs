﻿using System;

namespace WebStoreApp.Models
{
    public class Log
    {
        public int LogId { get; set; }

        public DateTime? RequestTime { get; set; }

        public string RequestUrl { get; set; }

        public string UserName { get; set; }

        public string Headers { get; set; }

        public string Body { get; set; }

        public string QueryString { get; set; }

        public string HttpVerb { get; set; }

        public string StatusCode { get; set; }

        public string StackTrace { get; set; }

        public string ErrorMessage { get; set; }
    }
}