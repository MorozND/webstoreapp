﻿using System;

namespace WebStoreApp.Models
{
    public class MenuItem
    {
        public string Name { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Active { get; set; }
    }
}