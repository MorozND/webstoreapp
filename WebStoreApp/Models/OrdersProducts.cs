﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WebStoreApp.Models
{
    public class OrdersProducts
    {
        [Key, Column(Order = 1)]
        public int OrderId { get; set; }

        [Key, Column(Order = 2)]
        public int ProductId { get; set; }

        public int ProductAmmount { get; set; }

        // navigation properties
        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }
    }
}