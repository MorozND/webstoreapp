﻿using System;
using Microsoft.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using WebStoreApp.App_Start;

namespace WebStoreApp.Models
{
    public class ApplicationUserManager : UserManager<AspNetUser>
    {
        public ApplicationUserManager(IUserStore<AspNetUser> store) 
            : base(store)
        { }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
                                                    IOwinContext context)
        {
            ApplicationContext db = context.Get<ApplicationContext>();
            var userManager = new ApplicationUserManager(new UserStore<AspNetUser>(db));

            userManager.RegisterTwoFactorProvider(
                    "Email code", 
                    new EmailTokenProvider<AspNetUser>
                    {
                        Subject = "Security code",
                        BodyFormat = "Your security code is {0}"
                    }
                );
            userManager.EmailService = new IdentityConfig.EmailService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                userManager.UserTokenProvider =
                    new DataProtectorTokenProvider<AspNetUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return userManager;
        }
    }
}