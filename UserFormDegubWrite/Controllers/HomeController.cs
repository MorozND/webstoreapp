﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserFormDegubWrite.Models;
using UserFormDegubWrite.ViewModels;

namespace UserFormDegubWrite.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(UserList.Users);
        }

        public ViewResult Add()
        {
            return View(new UserFormViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserFormViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Add", viewModel);
            }

            var user = new User
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Age = viewModel.Age ?? 0,
                Telephone = viewModel.Telephone
            };

            UserList.Users.Add(user);

            // output with Debug
            var stringBuilder = new StringBuilder("Полученные данные:");
            stringBuilder
                .Append($"\nId = {viewModel.Id}")
                .Append($"\nName = {viewModel.Name}")
                .Append($"\nAge = {viewModel.Age}")
                .Append($"\nTelephone = {viewModel.Telephone}");

            Debug.Write(stringBuilder.ToString());

            return RedirectToAction("Index");
        }
    }
}