﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using UserFormDegubWrite.Models;

namespace UserFormDegubWrite.ViewModels
{
    public class UserFormViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter the Name")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Enter the Age")]
        [Range(1, 100)]
        public byte? Age { get; set; }

        [Required(ErrorMessage = "Enter the Telephone")]
        [RegularExpression(@"^[\d\-\+\(\)]+$", ErrorMessage = "Enter the valid phone number")]
        public string Telephone { get; set; }

        public UserFormViewModel()
        { }

        public UserFormViewModel(User user)
        {
            Id = user.Id;
            Name = user.Name;
            Age = user.Age;
            Telephone = user.Telephone;
        }
    }
}