﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UserFormDegubWrite.Models;

namespace UserFormDegubWrite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // fill UserList
            UserList.Users = new List<User>
            {
                new User
                {
                    Id = 0,
                    Name = "Mary",
                    Age = 16,
                    Telephone = "111-11-11"
                },

                new User
                {
                    Id = 1,
                    Name = "Jack",
                    Age = 17,
                    Telephone = "222-22-22"
                },

                new User
                {
                    Id = 2,
                    Name = "Bob",
                    Age = 20,
                    Telephone = "333-33-33"
                },

                new User
                {
                    Id = 3,
                    Name = "Nikita",
                    Age = 23,
                    Telephone = "444-44-44"
                },

                new User
                {
                    Id = 4,
                    Name = "Steve",
                    Age = 22,
                    Telephone = "555-55-55"
                }
            };
        }
    }
}
