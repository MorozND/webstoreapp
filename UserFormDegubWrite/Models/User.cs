﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UserFormDegubWrite.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public byte Age { get; set; }

        public string Telephone { get; set; }
    }
}