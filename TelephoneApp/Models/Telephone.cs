﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TelephoneApp.Models
{
    public class Telephone
    {
        [Required]
        public byte Id { get; set; }

        [Required(ErrorMessage = "Enter Country code")]
        [RegularExpression(@"[\d]+", ErrorMessage = "The field can contain only numbers")]
        public string CountryCode { get; set; }

        [Required(ErrorMessage = "Enter Phone code")]
        [RegularExpression(@"[\d]+", ErrorMessage = "The field can contain only numbers")]
        public string PhoneCode { get; set; }

        [Required(ErrorMessage = "Enter Number")]
        [RegularExpression(@"[\d]+", ErrorMessage = "The field can contain only numbers")]
        public string Number { get; set; }
    }
}