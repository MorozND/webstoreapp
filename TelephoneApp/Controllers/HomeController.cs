﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelephoneApp.Models;

namespace TelephoneApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(TelephoneList.Telephones);
        }

        public ActionResult Info(int id)
        {
            var phone = TelephoneList.Telephones.SingleOrDefault(t => t.Id == id);

            if (phone == null)
                return HttpNotFound();

            return View(phone);
        }

        public ActionResult Edit(int id)
        {
            var phone = TelephoneList.Telephones.SingleOrDefault(t => t.Id == id);

            if (phone == null)
                return HttpNotFound();

            return View(phone);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Telephone phoneModel)
        {
            if (!ModelState.IsValid)
                return View("Edit", phoneModel);

            var phoneFromList = TelephoneList.Telephones.SingleOrDefault(t => t.Id == phoneModel.Id);

            if (phoneFromList == null)
                return HttpNotFound();

            // update properties
            phoneFromList.CountryCode = phoneModel.CountryCode;
            phoneFromList.PhoneCode = phoneModel.PhoneCode;
            phoneFromList.Number = phoneModel.Number;

            return RedirectToAction("Index");
        }
    }
}