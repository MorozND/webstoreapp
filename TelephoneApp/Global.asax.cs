﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TelephoneApp.Models;

namespace TelephoneApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            // fill TelephoneList
            TelephoneList.Telephones = new List<Telephone>
            {
                new Telephone
                {
                    Id = 0,
                    CountryCode = "375",
                    PhoneCode = "29",
                    Number = "1112233"
                },

                new Telephone
                {
                    Id = 1,
                    CountryCode = "375",
                    PhoneCode = "17",
                    Number = "2223344"
                },

                new Telephone
                {
                    Id = 2,
                    CountryCode = "375",
                    PhoneCode = "25",
                    Number = "3334455"
                },

                new Telephone
                {
                    Id = 3,
                    CountryCode = "375",
                    PhoneCode = "29",
                    Number = "4445566"
                },

                new Telephone
                {
                    Id = 4,
                    CountryCode = "375",
                    PhoneCode = "44",
                    Number = "5556677"
                }
            };
        }
    }
}
