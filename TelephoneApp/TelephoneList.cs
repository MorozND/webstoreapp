﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelephoneApp.Models;

namespace TelephoneApp
{
    public static class TelephoneList
    {
        public static List<Telephone> Telephones { get; set; }
    }
}